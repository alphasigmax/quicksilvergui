package com.quicksilver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUp extends Slidingactivity implements OnClickListener{

	EditText display_name;
	EditText username;
	EditText password;
	Button   signup;
	String full_name,user_name,pass;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.account_signup);
		display_name=(EditText) findViewById(R.id.fullname_edittext);
		username=(EditText)findViewById(R.id.username_edittext);
		password=(EditText)findViewById(R.id.password_edittext);
		signup=(Button)findViewById(R.id.signup_button);
		signup.setOnClickListener(this);
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.signup_button)
		{
			boolean Account_creation;
			full_name=display_name.getText().toString();
			user_name=username.getText().toString();
			pass=password.getText().toString();
			Account_creation=SignupService(full_name,user_name,pass);
			if(Account_creation)
			{
				Intent signin=new Intent(getApplicationContext(), Signin.class);
				startActivity(signin);
				//finish();
			}
			else {
				Toast.makeText(getApplicationContext(), "Signup Failed,Check Internet",Toast.LENGTH_SHORT).show();
			}
			
		}
		
	}
	
	public boolean SignupService(String name,String username,String pass)
	{
		return true;
	}
}
