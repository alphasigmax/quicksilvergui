package com.quicksilver;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Window;
import com.quicksilver.R;

public class SplashScreen extends SherlockActivity 
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		setTheme(R.style.Theme_Sherlock);
		super.onCreate(savedInstanceState);
		   requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splashscreen);

		Thread splash = new Thread() 
		{

			@Override
			public void run() 
			{
				// TODO Auto-generated method stub
				super.run();
				try 
				{
					sleep(2000);
				} 
				catch (InterruptedException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				finally 
				{

					Intent launcher = new Intent(getApplicationContext(),
							SignUp.class);
					startActivity(launcher);
					finish();
				}
			}
		};

		splash.start();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

}
