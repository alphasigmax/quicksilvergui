package com.quicksilver.ui.dialpad;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.quicksilver.R;
import com.quicksilver.Slidingactivity;

public class InCallActivity extends Slidingactivity {
	ImageView endcall;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.in_call_card);
		endcall = (ImageView) findViewById(R.id.endButton);
		endcall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
}
