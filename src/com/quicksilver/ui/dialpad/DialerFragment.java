package com.quicksilver.ui.dialpad;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DialerKeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.quicksilver.utils.Log;
import com.quicksilver.ui.dialpad.DialerLayout;
import com.quicksilver.ui.dialpad.DialerLayout.OnAutoCompleteListVisibilityChangedListener;
import com.quicksilver.widgets.DialerCallBar;
import com.quicksilver.widgets.DialerCallBar.OnDialActionListener;
import com.quicksilver.widgets.Dialpad.OnDialKeyListener;
import com.quicksilver.utils.PreferencesWrapper;
import com.quicksilver.widgets.Dialpad;
import com.quicksilver.utils.DialingFeedback;
import com.quicksilver.ui.dialpad.DigitsEditText;
import com.quicksilver.R;

public class DialerFragment extends SherlockFragment implements OnAutoCompleteListVisibilityChangedListener,OnDialKeyListener,OnDialActionListener,OnClickListener,OnLongClickListener,TextWatcher ,OnKeyListener{
	private DigitsEditText digits;
	private Boolean isDigit = false;
	private DialingFeedback dialFeedback;
	private Dialpad dialPad;
	private PreferencesWrapper prefsWrapper;
	private DialerCallBar callBar;
	private boolean mDualPane;

	private PhoneNumberFormattingTextWatcher digitFormater;

	private DialerLayout dialerLayout;
    private final static String TEXT_MODE_KEY = "text_mode";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mDualPane = getResources().getBoolean(R.bool.use_dual_panes);
		digitFormater = new PhoneNumberFormattingTextWatcher();
		setHasOptionsMenu(true);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.dialer_digit, container, false);
		digits = (DigitsEditText) v.findViewById(R.id.digitsText);
		dialPad = (Dialpad) v.findViewById(R.id.dialPad);
		callBar = (DialerCallBar) v.findViewById(R.id.dialerCallBar);
        dialerLayout = (DialerLayout) v.findViewById(R.id.top_digit_dialer);
        if(savedInstanceState != null) {
            isDigit = savedInstanceState.getBoolean(TEXT_MODE_KEY, isDigit);
        }
        
        digits.setOnEditorActionListener(keyboardActionListener);
        
        // Layout 
        dialerLayout.setForceNoList(mDualPane);
        dialerLayout.setAutoCompleteListVisibiltyChangedListener(this);
        dialPad.setOnDialKeyListener(this);
        callBar.setOnDialActionListener(this);
        callBar.setVideoEnabled(true);
        initButtons(v);
        v.setOnKeyListener(this);
		return v;
	}

	private final int[] buttonsToLongAttach = new int[] { R.id.button0,
			R.id.button1 };
	
    private OnEditorActionListener keyboardActionListener = new OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView tv, int action, KeyEvent arg2) {
            if (action == EditorInfo.IME_ACTION_GO) {
                placeCall();
                return true;
            }
            return false;
        }


    };

	@Override
	public void onAutoCompleteListVisibiltyChanged() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTrigger(int keyCode, int dialTone) {
		// TODO Auto-generated method stub
		 dialFeedback.giveFeedback(dialTone);
	        keyPressed(keyCode);
		
	}
    private void keyPressed(int keyCode) {
        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        digits.onKeyDown(keyCode, event);
    }
@Override
public void onAttach(Activity activity) {
	// TODO Auto-generated method stub
	super.onAttach(activity);
	 if (dialFeedback == null) {
         dialFeedback = new DialingFeedback(getActivity(), false);
     }

     dialFeedback.resume();
}
    @Override
    public void onDetach() {
    	// TODO Auto-generated method stub
    	dialFeedback.pause();
    	super.onDetach();
    }

	@Override
	public void placeCall() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(getActivity(), InCallActivity.class);
		startActivity(intent);
		
	}

	@Override
	public void placeVideoCall() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteChar() {
		// TODO Auto-generated method stub
		 keyPressed(KeyEvent.KEYCODE_DEL);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
        digits.getText().clear();

	}
    private void attachButtonListener(View v, int id, boolean longAttach) {
        ImageButton button = (ImageButton) v.findViewById(id);
        if(button == null) {
         //   Log.w(THIS_FILE, "Not found button " + id);
            return;
        }
        if(longAttach) {
            button.setOnLongClickListener(this);
        }else {
            button.setOnClickListener(this);
        }
    }
    private void initButtons(View v) {
        /*
        for (int buttonId : buttonsToAttach) {
            attachButtonListener(v, buttonId, false);
        }
        */
        for (int buttonId : buttonsToLongAttach) {
            attachButtonListener(v, buttonId, true);
        }

        digits.setOnClickListener(this);
        digits.setKeyListener(DialerKeyListener.getInstance());
        digits.addTextChangedListener(this);
        digits.setCursorVisible(false);
        afterTextChanged(digits.getText());
    }

	@Override
	public boolean onLongClick(View view) {
		  int vId = view.getId();
	        if (vId == R.id.button0) {
	            dialFeedback.hapticFeedback();
	            keyPressed(KeyEvent.KEYCODE_PLUS);
	            return true;
	        }else if(vId == R.id.button1) {
	            if(digits.length() == 0) {
	               // placeVMCall();
	                return true;
	            }
	        }
	        return false;
	}

	@Override
	public void onClick(View view) {
		 int viewId = view.getId();
	        /*
	        if (view_id == R.id.switchTextView) {
	            // Set as text dialing if we are currently digit dialing
	            setTextDialing(isDigit);
	        } else */
	        if (viewId == digits.getId()) {
	            if (digits.length() != 0) {
	                digits.setCursorVisible(true);
	            }
	        }
		
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		 final boolean notEmpty = digits.length() != 0;
	        //digitsWrapper.setBackgroundDrawable(notEmpty ? digitsBackground : digitsEmptyBackground);
	        callBar.setEnabled(notEmpty);

	        if (!notEmpty && isDigit) {
	            digits.setCursorVisible(false);
	        }
	       // applyTextToAutoComplete();
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
        KeyEvent event1 = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        
        return digits.onKeyDown(keyCode, event1);
	}
}
