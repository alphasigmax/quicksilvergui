package com.quicksilver.ui;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.quicksilver.ui.calllog.CallLogListFragment;
import com.quicksilver.ui.favorites.FavListFragment;
import com.quicksilver.ui.messages.ConversationsListFragment;
import com.quicksilver.utils.Log;
import com.quicksilver.utils.CustomDistribution;
import com.quicksilver.ui.Siphome;
import com.quicksilver.ui.dialpad.DialerFragment;
import com.quicksilver.R;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

public class Siphome extends SherlockFragmentActivity/*
													 * implements
													 * ActionBar.TabListener
													 */{
	private boolean mDualPane;
	Integer initTabId = null;
	private ViewPager mViewPager;
	private TabsAdapter mTabsAdapter;
	private final static int TAB_ID_DIALER = 0;
	private final static int TAB_ID_CALL_LOG = 1;
	private final static int TAB_ID_FAVORITES = 2;
	private final static int TAB_ID_MESSAGES = 3;
	private final static int TAB_ID_WARNING = 4;
	private DialerFragment mDialpadFragment;
	public interface ViewPagerVisibilityListener {
		void onVisibilityChanged(boolean visible);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sip_home);
		/*
		 * if (findViewById(R.id.fragment_container) != null) {
		 * 
		 * // However, if we're being restored from a previous state, // then we
		 * don't need to do anything and should return or else // we could end
		 * up with overlapping fragments. if (savedInstanceState != null) {
		 * return; }
		 * 
		 * // Create an instance of ExampleFragment
		 * 
		 * DialerFragment dial_instance = new DialerFragment();
		 * 
		 * // In case this activity was started with special instructions from
		 * an Intent, // pass the Intent's extras to the fragment as arguments
		 * dial_instance.setArguments(getIntent().getExtras());
		 * 
		 * // Add the fragment to the 'fragment_container' FrameLayout
		 * getSupportFragmentManager
		 * ().beginTransaction().add(R.id.fragment_container
		 * ,dial_instance).commit(); }
		 */
		mDualPane = getResources().getBoolean(R.bool.use_dual_panes);
		final ActionBar ab = getSupportActionBar();
		ab.setDisplayShowHomeEnabled(false);
		ab.setDisplayShowTitleEnabled(false);
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Tab dialerTab = ab.newTab()
				.setContentDescription(R.string.dial_tab_name_text)
				.setIcon(R.drawable.ic_ab_dialer_holo_dark);
		Tab callLogTab = ab.newTab()
				.setContentDescription(R.string.calllog_tab_name_text)
				.setIcon(R.drawable.ic_ab_history_holo_dark);
		Tab favoritesTab = null;

		if (CustomDistribution.supportFavorites()) {
			favoritesTab = ab.newTab()
					.setContentDescription(R.string.favorites_tab_name_text)
					.setIcon(R.drawable.ic_ab_favourites_holo_dark);
		}
		Tab messagingTab = null;
		if (CustomDistribution.supportMessaging()) {
			messagingTab = ab.newTab()
					.setContentDescription(R.string.messages_tab_name_text)
					.setIcon(R.drawable.ic_ab_text_holo_dark);
		}
		mViewPager = (ViewPager) findViewById(R.id.pager);
        mTabsAdapter = new TabsAdapter(this, getSupportActionBar(), mViewPager);
        mTabsAdapter.addTab(dialerTab, DialerFragment.class, TAB_ID_DIALER);
        mTabsAdapter.addTab(callLogTab, CallLogListFragment.class, TAB_ID_CALL_LOG);
        
        if(favoritesTab != null)
        {
            mTabsAdapter.addTab(favoritesTab, FavListFragment.class, TAB_ID_FAVORITES);
        }
        if (messagingTab != null) {
            mTabsAdapter.addTab(messagingTab, ConversationsListFragment.class, TAB_ID_MESSAGES);
        }
        
		/*
		 * dialerTab.setTabListener(this); callLogTab.setTabListener(this);
		 * favoritesTab.setTabListener(this); messagingTab.setTabListener(this);
		 * getSupportActionBar().addTab(dialerTab);
		 * getSupportActionBar().addTab(callLogTab);
		 * getSupportActionBar().addTab(favoritesTab);
		 * getSupportActionBar().addTab(messagingTab);
		 */
	}

	/*
	 * @Override public void onTabSelected(Tab tab, FragmentTransaction ft) { //
	 * TODO Auto-generated method stub
	 * if(tab.getContentDescription().equals(R.string.dial_tab_name_text)) {
	 * 
	 * } else if
	 * (tab.getContentDescription().equals(R.string.calllog_tab_name_text)) {
	 * 
	 * } else if
	 * (tab.getContentDescription().equals(R.string.favorites_tab_name_text)) {
	 * 
	 * } else if
	 * (tab.getContentDescription().equals(R.string.messages_tab_name_text)) {
	 * 
	 * } }
	 * 
	 * @Override public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	 * // TODO Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void onTabReselected(Tab tab, FragmentTransaction ft) {
	 * // TODO Auto-generated method stub
	 * 
	 * }
	 */
	private class TabsAdapter extends FragmentPagerAdapter implements
			ViewPager.OnPageChangeListener, ActionBar.TabListener {
		private final Context mContext;
		private final ActionBar mActionBar;
		private final ViewPager mViewPager;
		private final List<String> mTabs = new ArrayList<String>();
		private final List<Integer> mTabsId = new ArrayList<Integer>();
		private boolean hasClearedDetails = false;

		private int mCurrentPosition = -1;
		/**
		 * Used during page migration, to remember the next position
		 * {@link #onPageSelected(int)} specified.
		 */
		private int mNextPosition = -1;

		public TabsAdapter(FragmentActivity activity, ActionBar actionBar,
				ViewPager pager) {
			super(activity.getSupportFragmentManager());
			mContext = activity;
			mActionBar = actionBar;
			mViewPager = pager;
			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
		}

		public void addTab(ActionBar.Tab tab, Class<?> clss, int tabId) {
			mTabs.add(clss.getName());
			mTabsId.add(tabId);
			mActionBar.addTab(tab.setTabListener(this));
			notifyDataSetChanged();
		}

		public void removeTabAt(int location) {
			mTabs.remove(location);
			mTabsId.remove(location);
			mActionBar.removeTabAt(location);
			notifyDataSetChanged();
		}

		public Integer getIdForPosition(int position) {
			if (position >= 0 && position < mTabsId.size()) {
				return mTabsId.get(position);
			}
			return null;
		}

		public Integer getPositionForId(int id) {
			int fPos = mTabsId.indexOf(id);
			if (fPos >= 0) {
				return fPos;
			}
			return null;
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}

		@Override
		public Fragment getItem(int position) {
			return Fragment.instantiate(mContext, mTabs.get(position),
					new Bundle());
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			clearDetails();
			if (mViewPager.getCurrentItem() != tab.getPosition()) {
				mViewPager.setCurrentItem(tab.getPosition(), true);
			}
		}

		@Override
		public void onPageSelected(int position) {
			mActionBar.setSelectedNavigationItem(position);

			if (mCurrentPosition == position) {
				/*
				 * Log.w(THIS_FILE,
				 * "Previous position and next position became same (" +
				 * position + ")");
				 */
			}

			mNextPosition = position;
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// Nothing to do
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			// Nothing to do
		}

		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
			// Nothing to do
		}

		/*
		 * public void setCurrentPosition(int position) { mCurrentPosition =
		 * position; }
		 */

		@Override
		public void onPageScrollStateChanged(int state) {
			switch (state) {
			case ViewPager.SCROLL_STATE_IDLE: {
				if (mCurrentPosition >= 0) {
					sendFragmentVisibilityChange(mCurrentPosition, false);
				}
				if (mNextPosition >= 0) {
					sendFragmentVisibilityChange(mNextPosition, true);
				}
				supportInvalidateOptionsMenu();

				mCurrentPosition = mNextPosition;
				break;
			}
			case ViewPager.SCROLL_STATE_DRAGGING:
				clearDetails();
				hasClearedDetails = true;
				break;
			case ViewPager.SCROLL_STATE_SETTLING:
				hasClearedDetails = false;
				break;
			default:
				break;
			}
		}

		private void clearDetails() {
			if (mDualPane && !hasClearedDetails) {
				FragmentTransaction ft = Siphome.this
						.getSupportFragmentManager().beginTransaction();
				ft.replace(R.id.details, new Fragment(), null);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}
		}
	}

	private void sendFragmentVisibilityChange(int position, boolean visibility) {
		try {
			final Fragment fragment = getFragmentAt(position);
			if (fragment instanceof ViewPagerVisibilityListener) {
				((ViewPagerVisibilityListener) fragment)
						.onVisibilityChanged(visibility);
			}
		} catch (IllegalStateException e) {
			/* Log.e(THIS_FILE, "Fragment not anymore managed"); */
		}
	}

	private Fragment getFragmentAt(int position) {
		Integer id = mTabsAdapter.getIdForPosition(position);
		if (id != null) {
			if (id == TAB_ID_DIALER) {
				return mDialpadFragment;
			} else if (id == TAB_ID_CALL_LOG) {
				//return mCallLogFragment;
			} else if (id == TAB_ID_MESSAGES) {
				//return mMessagesFragment;
			} else if (id == TAB_ID_FAVORITES) {
			//	return mPhoneFavoriteFragment;
			} else if (id == TAB_ID_WARNING) {
				//return mWarningFragment;
			}
		}
		throw new IllegalStateException("Unknown fragment index: " + position);
	}
	
}
