package com.quicksilver.ui.messages;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.quicksilver.R;
import com.quicksilver.widgets.CSSListFragment;

public class ConversationsListFragment extends CSSListFragment{
	 private View mHeaderView;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
        ListView lv = getListView();

        if(getListAdapter() == null && mHeaderView != null) {
            lv.addHeaderView(mHeaderView, null, true);
        }
        
        lv.setOnCreateContextMenuListener(this);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view=inflater.inflate(R.layout.message_list_fragment,container,false);
		ListView lv = (ListView) view.findViewById(android.R.id.list);
		mHeaderView = (ViewGroup)
                inflater.inflate(R.layout.conversation_list_item, lv, false);
		return view;
	}
	@Override
	public Loader<Cursor> onCreateLoader(int loader, Bundle args) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void changeCursor(Cursor c) {
		// TODO Auto-generated method stub
		
	}

}
