package com.quicksilver;

import com.quicksilver.ui.Siphome;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Signin extends Slidingactivity implements OnClickListener{
	
	EditText Usename;
	EditText Password;
	Button Signin;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.account_signin);
		Usename=(EditText)findViewById(R.id.username);
		Password=(EditText)findViewById(R.id.password);
		Signin=(Button)findViewById(R.id.signin_button);
		Signin.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.signin_button)
		{
			Intent home=new Intent(getApplicationContext(), Siphome.class);
			startActivity(home);
		}
		
	}

}
